'use strict';
var MongoClient = require('mongodb').MongoClient;

let atlas_connection_uri;
let cachedDb = null;

let firebase = require('firebase');

exports.handler = function(event, context,callback)
{


    // var uri = process.env['MONGODB_ATLAS_CLUSTER_URI'];
    var uri = "mongodb://163.172.131.83:28018/harmanconnectedcar-180411";
    context.callbackWaitsForEmptyEventLoop = false;
    firebase.initializeApp({
        serviceAccount: {},
        databaseURL: "https://harmanconnectedcar-180411.firebaseio.com/"
    });


    var eventDataRef = firebase.database().ref('events');
    eventDataRef.on('child_changed', function(child,prev) {
        // all records after the last continue to invoke this function
        console.log("*************snapshot*****snapshot*****************")
        var lastKey = Object.keys(child.val()).sort().reverse()[0];
        var eventDetectedData = child.val()[lastKey];
        console.log(eventDetectedData);
        console.log(eventDetectedData);
        if (atlas_connection_uri != null) {
         processEvent(event, context, callback,eventDetectedData,"events");
         }
         else {
         atlas_connection_uri = uri;
         console.log('the Atlas connection string is ' + atlas_connection_uri);
         processEvent(event, context, callback,eventDetectedData,"events");
         }

    });

    var gpslogsRef= firebase.database().ref('gpsLogs');
    gpslogsRef.on('child_changed', function(child,prev) {
        // all records after the last continue to invoke this function
        console.log("*************seconddddd*****snapshot*****************")
        var lastKey = Object.keys(child.val()).sort().reverse()[0];
        var gpsLogsDetectedData = child.val()[lastKey];
        console.log(gpsLogsDetectedData);
        if (atlas_connection_uri != null) {
            processEvent(event, context, callback,gpsLogsDetectedData,"gpsLogs");
        }
        else {
            atlas_connection_uri = uri;
            console.log('the Atlas connection string is ' + atlas_connection_uri);
            processEvent(event, context, callback,gpsLogsDetectedData,"gpsLogs");
        }


    });


};


function processEvent(event, context, callback,triggerData,tableName) {
    console.log('Calling MongoDB Atlas from AWS Lambda with event: ' + JSON.stringify(event));

    var jsonContents = JSON.parse(JSON.stringify(triggerData));

    //the following line is critical for performance reasons to allow re-use of database connections across calls to this Lambda function and avoid closing the database connection. The first call to this lambda function takes about 5 seconds to complete, while subsequent, close calls will only take a few hundred milliseconds.
    context.callbackWaitsForEmptyEventLoop = false;

    try {
        if (cachedDb == null) {
            console.log('=> connecting to database');
            MongoClient.connect(atlas_connection_uri, function (err, db) {
                cachedDb = db;
                return createDoc(db, jsonContents, callback,tableName);
            });
        }
        else {
            createDoc(cachedDb, jsonContents, callback,tableName);
        }
    }
    catch (err) {
        console.error('an error occurred', err);
    }
}

function createDoc (db, json, callback,tableName) {
    db.collection(tableName).insertOne( json, function(err, result) {
        if(err!=null) {
            console.error("an error occurred in createDoc", err);
            callback(null, JSON.stringify(err));
        }
        else {
            console.log("yess! You just created an entry into the restaurants collection with id: " + result.insertedId);
            // callback(null, "SUCCESS");
        }
        //we don't need to close the connection thanks to context.callbackWaitsForEmptyEventLoop = false (above)
        //this will let our function re-use the connection on the next called (if it can re-use the same Lambda container)
        //db.close();
    });
};

